import React,{Component} from 'react';
import './QuoteAdd.css';

import axios from 'axios';

class QuoteAdd extends Component{
    state = {
        quote:{
            title:'',
            body:''
        },
        loading: false
    };

    quoteCreateHandler = event =>{
        event.preventDefault();

        this.setState({loading:true});

        axios.post('/control8.json', this.state.quote).then(() => {
            this.setState({loading:false});
            this.props.history.replace('/');
        });
    };

    quoteValueChanged = event => {
        event.persist()
        this.setState(prevState => {
            return {
                quote: {...prevState.quote, [event.target.name]: event.target.value}
            };
        });
    };


    render(){
        return(
            <form className="QuoteAdd">
                <input type="text" name="title" placeholder="Enter author"
                       value={this.state.quote.title} onChange={this.quoteValueChanged} />
                <textarea name="body" placeholder="Enter new Quote"
                          value={this.state.quote.body} onChange={this.quoteValueChanged} />
                <button onClick={this.quoteCreateHandler}>Create</button>
            </form>
        )
    }
}

export default QuoteAdd;