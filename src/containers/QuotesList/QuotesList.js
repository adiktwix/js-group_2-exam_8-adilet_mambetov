import React, {Component, Fragment} from 'react';
import QuoteShortDesc from "../../components/Quotes/QuoteShortDesc/QuoteShortDesc";
import axios from 'axios';

class QuotesList extends Component {
    state = {
        quotes: []
    };

    componentDidMount(){
        axios.get('/control8.json').then((response) =>{
            console.log(response);
            const quotes = [];

            for (let key in response.data){
                quotes.push({...response.data[key], id: key})
            }
            this.setState({quotes});
        });
    };

    quoteDeleteHandler = id => {
        axios.delete(`/control8/${id}.json`).then(() => {
            this.setState(prevState => {
                const quotes = [...prevState.quotes];
                const index = quotes.findIndex(quote => quote.id === id);
                quotes.splice(index, 1);
                return {quotes}
            });
        })
    };


    render(){
        return (
            <Fragment>
                <div className="QuotesList">
                    {this.state.quotes.map(quote => (
                        <QuoteShortDesc
                            key={quote.id} title={quote.title} id={quote.id}
                            deleted = {() => this.quoteDeleteHandler(quote.id)}
                        />
                        ))}
                </div>
            </Fragment>
        )
    }
}

export default QuotesList;