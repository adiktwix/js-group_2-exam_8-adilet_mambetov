import React, {Component, Fragment} from "react";
import axios from 'axios';

import './QuoteFull.css';



class QuoteFull extends Component {
    state = {
        loadedPost: null
    }

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/control8/${id}.json`).then((response) => {
            this.setState({loadedPost: response.data});
        })
    }

    render(){
        if (this.state.loadedPost){
            return(
                <div className="QuoteFull">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                </div>
            );
        } else {
            return <p>Loading...</p>;
        }
    }
}

export default QuoteFull;