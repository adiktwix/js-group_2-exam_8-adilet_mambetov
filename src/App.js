import React, {Component, Fragment} from 'react';
import {NavLink, Route, Switch} from "react-router-dom";
import QuotesList from "./containers/QuotesList/QuotesList";
import QuoteAdd from "./containers/QuoteAdd/QuoteAdd";
import './app.css'
import QuoteFull from "./containers/QuoteFull/QuoteFull";

class App extends Component {
  render() {
    return (
        <Fragment>
        <nav className="App-nav">
            <ul>
                <li><NavLink to="/" exact>Quotes</NavLink></li>
                <li><NavLink to="/add" exact>Add new quote</NavLink></li>
            </ul>
        </nav>
        <Switch>
            <Route path="/" exact component={QuotesList} />
            <Route path="/add" component={QuoteAdd} />
            <Route path="/quotes/:id" component={QuoteFull} />
            <Route render={() => <h1>Not found</h1>}/>
        </Switch>
        </Fragment>
    );
  }
}

export default App;
