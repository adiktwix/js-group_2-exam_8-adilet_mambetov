import React from 'react';
import './QuoteShortDesc.css';
import {Link} from "react-router-dom";

const QuoteShortDesc = props => (
    <div className="QuoteShortDesc">
        <h4>
            <Link to={'/quotes/' + props.id}>{props.title}</Link>
            <button onClick={props.deleted}>Delete</button>
        </h4>
    </div>
);

export default QuoteShortDesc;